" Plugin manager
call plug#begin()

{% for item in neovim_plugins %}
Plug '{{ item }}'
{% endfor %}

call plug#end()

" General settings
set nocompatible " no old school vi support
set nobackup	 " don't backup edited buffers
set noswapfile	 " don't create a swap file
set autoread     " reload externally edited files
set visualbell 	 " silence beeps
set noerrorbells " silence beeps

" Windows / buffers
set hidden     " hide buffers rather than require they are saved or discarded
set splitbelow " open horizontal splits below the current file
set splitright " open vertical splits to the right of the current file

" Searching
set incsearch  " show search matches as they are typed
set ignorecase " ignore case when searching
set smartcase  " make searches case-sensitive when uppercase is used

" Editing
set number       " enable line numbers
set cursorline   " highlight the current line
set ruler        " display the cursor position in STATUS bar
set showcmd      " show partial commands in the STATUS bar
set textwidth=0  " don't wrap
set wrapmargin=0 " don't wrap
set scrolloff=12 " number of lines to display either side of the current
set backspace=indent,eol,start " fix backspace

" Indentation
filetype plugin indent on
set shiftwidth=4  " number of spaces to use when indenting in CMD mode
set shiftround    " round to the closest mulitple of shiftwidth in CMD mode
set expandtab     " convert tabs to spaces in INSERT mode
set softtabstop=4 " number of columns a tab uses

" Visual settings
if (has("termguicolors"))
    set termguicolors
endif
syntax enable " enable syntax highlighting
autocmd BufRead,BufNewFile *.twig set filetype=htmljinja " syntax highlighting for Twig
set background=dark
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light='soft'
silent! colorscheme srcery

" CtrlP
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/](\.git|tmp|cache|log|vendor|target|report)$',
    \ 'file': '\v\.(so|swp|zip|log)$'
    \ }
let g:ctrlp_working_path_mode = 'a'   " use the directory of the current file
let g:ctrlp_clear_cache_on_exit= 1     " maintain cache between vim sessions
let g:ctrlp_open_multiple_files = 'i' " open multiple files as hidden buffers
let g:ctrlp_mruf_max = 15             " limit the number of recently-used files CtrlP should remember
let g:ctrlp_user_command = ['.git/', 'cd %s && git ls-files --exclude-standard -co'] " faster indexing

" Vim Markdown
let g:vim_markdown_folding_disabled=1 " disable folding

" PHP syntax
let g:php_ignore_phpdoc=1

" Airline
let g:airline_theme='srcery'
let g:airline_section_c = '%F' " display full filepath
set laststatus=2 " always visible

" Clojure Static
let g:clojure_align_multiline_strings = 1
