# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Append (rather than overwrite) commands to the HISTFILE
shopt -s histappend

# Reduce noise/repetition
HISTCONTROL=ignoreups:ignorespace

# Number of commands transferred to HISTFILE when the shell exits
HISTSIZE=1000

# Number of lines permitted in HISTFILE
HISTFILESIZE=2000

# Check the window size after each command; update the values of LINES and
# COLUMNS, if necessary
shopt -s checkwinsize

# Handy aliases
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases

# Ensure a default EDITOR is defined
export EDITOR="nvim"

# Better prompt
PS1="\u@\H:\w\[\e[32;40m\]\$(__git_ps1)\[\e[0m\]\$ "

# Better color support
export TERM=tmux-256color
export CLICOLOR=1

# Bash completion
if [[ -f /etc/bash_completion ]]; then
    . /etc/bash_completion
fi

# Git helper functions
function push() {
  BRANCH=`git branch -l | grep "*" | sed 's/\* //'`
  ARGS=("$@")
  CMD="git push origin $BRANCH $ARGS"
  history -s $CMD
  (set -x; $CMD)
}
function pull() {
  BRANCH=`git branch -l | grep "*" | sed 's/\* //'`
  ARGS=("$@")
  CMD="git pull origin $BRANCH --rebase $ARGS"
  history -s $CMD
  (set -x; $CMD)
}
