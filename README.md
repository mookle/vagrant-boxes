# Vagrant boxes

Vagrant / Ansible setup to create VM boxes for local development use.

## Creating / packaging boxes

Nothing out of the ordinary here:

```bash
$ vagrant up
$ vagrant package --output ../_boxes/{BOX_NAME}.box
$ vagrant box add {BOX_REGISTRY_NAME} ../_boxes/{BOX_NAME}.box
$ vagrant destroy
```

## Boxes

Each box is as stripped back as possible, to be customised on a project-by-project basis, but containing the core components to enable development regardless of host setup. Nothing about the package choice or configuration is designed to be widely applicable. It's about my personal dev setup; if it works for you too, great!

A brief rundown of the packages installed with each box:

#### Base / core box:

- Debian 9
- tmux
- Git
- cURL
- Neovim

#### LAMP stack:

- Apache2
- MariaDB
- PHP 7
- Composer


Check out the relevant `Vagrantfile` or `ansible/playbook.yml` for more information.
